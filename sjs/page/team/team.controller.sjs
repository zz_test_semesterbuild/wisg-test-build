/*
        WISG - make work a game
        Copyright (C) 2020  Eric Fischer and all contributors

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

class TeamController {

  constructor() {
    this.utils = new Utils();
    this.lsm = new LocalStorageManager();
    this.log = new Logger("TeamController");
    this.userService = new UserService();
    this.teamService = new TeamService();
  }

  addTeam(principal, name, successCallback, errorCallback) {
    let header = this.utils.createAjaxHeader();
    let request = {
      principal: principal,
      name: name
    };
    let ref = this;
    $.ajax({
      url: ref.lsm.getServerUrl() + "/api/0.1/team/create",
      method: "POST",
      datatype: "json",
      data: request,
      headers: header,
      success: function(data, status, xhr) {
        var response = JSON.parse(data);
        ref.log.info("Team created.");
        successCallback(response);
      },
      error: function(xhr, status, error) {
        ref.log.warn("Team not created.");
        errorCallback();
      }
    });
  }

  whoami(callback) {
    let ref = this;
    this.userService.whoami(function(user) {
      callback(user);
    }, function() {
      ref.userService.logout();
      new LoginView();
    });
  }

  addUserToTeam(userId, teamId, successCallback, errorCallback) {
    let header = this.utils.createAjaxHeader();
    let request = {
      user: userId,
      team: teamId
    };
    let ref = this;
    $.ajax({
      url: ref.lsm.getServerUrl() + "/api/0.1/team/assign",
      method: "POST",
      datatype: "json",
      data: request,
      headers: header,
      success: function(data, status, xhr) {
        ref.log.info("User added to team.");
        successCallback();
      },
      error: function(xhr, status, error) {
        ref.log.error("User not added to team.");
        errorCallback();
      }
    });
  }

  checkRights() {
    return this.userService.checkSession();
  }

  fetchTeams(successCallback, errorCallback) {
    this.teamService.fetchTeams(successCallback, errorCallback);
  }

  fetchUsers(principal, successCallback, errorCallback) {
    this.userService.fetchUsers(principal, successCallback, errorCallback);
  }

}
