<?php

/*
        WISG - make work a game
        Copyright (C) 2020  Eric Fischer and all contributors

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

include_once 'config.inc.php';
include_once 'install/internal/installer.php';

$configured = DATABASE_URL != "" && DATABASE_NAME != ""
                && DATABASE_USER != "" && TABLE_PREFIX != "";

$installer = new Installer();
if(isset($_GET["install"]) && $_GET["install"] == true && $configured == true) {
  $installer->install();
}

?>

<html>

  <head>
    <title>Installation</title>
  </head>

  <body>
    <h1>Willkommen bei der Installation von "Work Is A Game"!</h1>
    <?php if(!is_writable('logs/logs.log')) : ?>
      <p>
        Bitte stellen Sie sicher, dass die Log-Datei unter <i>logs/logs.log</i>
        vom System geschrieben werden kann.
      </p>
    <?php endif; ?>
    <?php if(!$configured) : ?>
      <p>
        Bitte konfigurieren Sie das System in der Datei <i>config.inc.php</i>
        bevor Sie mit der Installation beginnen.
      </p>
    <?php elseif($configured && !isset($_GET["install"]) && is_writable('logs/logs.log')) : ?>
      <form action="/install" method="get">
        <button type="submit" name="install" value="true">Jetzt installieren</button>
      </form>
    <?php elseif($installer->is_successful() == true) : ?>
      <p>
        Installation erfolgreich! Viel Spaß beim Spielen.
      </p>
      <p>
        Sie können sich nun als <i>admin</i> mit dem Passwort <i>TheAdmin</i>
        anmelden.
        <strong>Bitte ändern Sie dieses Passwort unverzüglich!</strong>
      </p>
    <?php endif; ?>
  </body>

</html>
