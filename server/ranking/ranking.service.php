<?php

/*
        WISG - make work a game
        Copyright (C) 2020  Eric Fischer and all contributors

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

include_once "logs/logger.php";
include_once "user/user.service.php";
include_once "team/team.service.php";
include_once "ranking/ranking.dal.php";

class RankingService {

  private Logger $logger;
  private UserService $user_service;
  private TeamService $team_service;
  private RankingDal $dal;

  public function __construct() {
    $this->logger = new Logger("RankingService");
    $this->dal = new RankingDal();
    $this->user_service = new UserService();
    $this->team_service = new TeamService();
  }

  public function create_ranking(int $user_id, int $team_id) {
    $user = $this->user_service->fetch_user($user_id);
    $team = $this->team_service->fetch_team($user_id, $team_id);
    $points = array();
    foreach ($team->get_members() as $member) {
      $this->logger->debug("Fetching points for user: $member");
      $points[] = array(
        "user" => $member,
        "points" => $this->fetch_points_of_user($member),
      );
    }
    $this->logger->debug("Sorting array: ".json_encode($points));
    function rank_users($a, $b) {
      return $b["points"] - $a["points"];
    }
    usort($points, "rank_users");
    $this->logger->debug("Sorted array: ".json_encode($points));
    return $points;
  }

  private function fetch_points_of_user(int $user) {
    $sum = 0;
    foreach ($this->dal->fetch_completed_tasks($user) as $task) {
      $task_sum = $task["xp"];
      $absolute = $this->dal->fetch_absolute_boni($task["id"]);
      $relative = $this->dal->fetch_relative_boni($task["id"]);
      foreach ($absolute as $abs_val) {
        $task_sum += $abs_val;
      }
      foreach($relative as $rel_val) {
        $task_sum = $task_sum * $rel_val;
      }
      $sum += $task_sum;
    }
    return $sum;
  }

}

 ?>
