<?php

/*
        WISG - make work a game
        Copyright (C) 2020  Eric Fischer and all contributors

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

include_once "bonus/bonus.service.php";
include_once "logs/logger.php";
include_once "exceptions/missing_parameters.exception.php";
include_once "exceptions/parameter_type.exception.php";
include_once "util/token.util.php";

class BonusController {

  private Logger $logger;
  private BonusService $service;
  private TokenUtil $token_util;

  public function __construct() {
    $this->logger = new Logger("BonusController");
    $this->service = new BonusService();
    $this->token_util = new TokenUtil();
  }

  public function create_bonus() {
    $user_id = $this->token_util->check_token();

    if(!isset($_POST["name"]))
      throw new MissingParametersException("No name for bonus given.");
    if(!isset($_POST["type"]))
      throw new MissingParametersException("No type for bonus given.");
    if(!isset($_POST["value"]))
      throw new MissingParametersException("No value for bonus given.");

    $name = $_POST["name"];
    if(gettype($name) != "string")
      throw new ParameterTypeException("Name not a string.");
    $type = $_POST["type"];
    if(gettype($type) != "string" || ("ABSOLUTE" != $type && "RELATIVE" != $type))
      throw new ParameterTypeException("Type not right specified.");
    $value = doubleval($_POST["value"]);
    $this->logger->debug("Value: ".$value." (".gettype($value).")");
    if(gettype($value) != "double")
      throw new ParameterTypeException("Value not a valid number");

    $principal = null;
    if(isset($_POST["principal"])) {
      $principal = intval($_POST["principal"]);
      if(gettype($principal) != "integer")
        throw new ParameterTypeException("Principal not a number.");
    }

    $created = $this->service->create_bonus($user_id, $name, $type, $value, $principal);

    $output_json = array(
      "id" => $created->get_id(),
      "principal" => $created->get_principal(),
      "name" => $created->get_name(),
      "value" => $created->get_value(),
      "type" => $created->get_type()
    );

    echo json_encode($output_json);
  }

  public function fetch_boni() {
    $user_id = $this->token_util->check_token();

    $principal = null;
    if(isset($_POST["principal"])) {
      $principal = intVal($_POST["principal"]);
      if(gettype($principal) != "integer")
        throw new ParameterTypeException("Principal not a number");
    }

    $fetched = $this->service->fetch_boni($user_id, $principal);

    $output_json = array();

    foreach ($fetched as $bonus) {
      $bonus_json = array(
        "id" => $bonus->get_id(),
        "principal" => $bonus->get_principal(),
        "name" => $bonus->get_name(),
        "value" => $bonus->get_value(),
        "type" => $bonus->get_type()
      );
      $output_json[] = $bonus_json;
    }

    echo json_encode($output_json);
  }

}

 ?>
